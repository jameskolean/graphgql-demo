package com.codegreenllc.graphqldemo.entities;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

import lombok.Data;

@Data
@Entity
//@Table(name = "AUTHOR")
public class Author {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private UUID id;
	private String imageUrl;
	private String name;
	@Version
	private Long version;
}