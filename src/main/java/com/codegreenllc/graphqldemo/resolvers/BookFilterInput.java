package com.codegreenllc.graphqldemo.resolvers;

import lombok.Data;

@Data
public class BookFilterInput {
	StringFilterInput title, category;
}
