package com.codegreenllc.graphqldemo.resolvers;

import java.util.UUID;

import org.springframework.stereotype.Component;

import graphql.language.StringValue;
import graphql.schema.Coercing;
import graphql.schema.CoercingParseLiteralException;
import graphql.schema.CoercingParseValueException;
import graphql.schema.CoercingSerializeException;
import graphql.schema.GraphQLScalarType;

@Component
public class UUIDScalarType extends GraphQLScalarType {

	UUIDScalarType() {
		super("UUID", "UUID value", new Coercing<UUID, String>() {

			@Override
			public UUID parseLiteral(final Object input) throws CoercingParseLiteralException {
				return UUID.fromString(((StringValue) input).getValue());
			}

			@Override
			public UUID parseValue(final Object input) throws CoercingParseValueException {
				return UUID.fromString(input.toString());
			}

			@Override
			public String serialize(final Object dataFetcherResult) throws CoercingSerializeException {
				return ((UUID) dataFetcherResult).toString();
			}

		});
	}

}