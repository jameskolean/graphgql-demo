package com.codegreenllc.graphqldemo.resolvers;

import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.codegreenllc.graphqldemo.entities.Book;
import com.codegreenllc.graphqldemo.repositories.BookRepository;
import com.coxautodev.graphql.tools.GraphQLMutationResolver;

@Component
public class Mutation implements GraphQLMutationResolver {

	@Autowired
	BookRepository bookRepository;

	public Book addBook(final String title, final String textUrl, final String category) {
		final Book book = new Book();
		book.setTitle(title);
		book.setCategory(category);
		book.setTextUrl(textUrl);
		return bookRepository.save(book);
	}

	protected boolean isBlank(final String target) {
		return target == null || target.isEmpty();
	}

	public Boolean removeBook(final UUID id) {
		bookRepository.deleteById(id);
		return true;
	}

	public Book updateBook(final UUID id, final String title, final String textUrl, final String category) {
		final Optional<Book> optionalBook = bookRepository.findById(id);
		if (!optionalBook.isPresent()) {
			return null;
		}
		final Book book = optionalBook.get();
		if (!isBlank(title)) {
			book.setTitle(title);
		}
		if (!isBlank(category)) {
			book.setCategory(category);
		}
		if (!isBlank(textUrl)) {
			book.setTextUrl(textUrl);
		}
		return bookRepository.save(book);
	}
}