package com.codegreenllc.graphqldemo.resolvers;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import com.codegreenllc.graphqldemo.entities.Author;
import com.codegreenllc.graphqldemo.entities.Book;
import com.codegreenllc.graphqldemo.repositories.AuthorRepository;
import com.codegreenllc.graphqldemo.repositories.BookRepository;
import com.coxautodev.graphql.tools.GraphQLQueryResolver;

@Component
public class Query implements GraphQLQueryResolver {
	@Autowired
	AuthorRepository authorRepository;

	@Autowired
	BookRepository bookRepository;

	public Iterable<Author> authors() {
		return authorRepository.findAll();
	}

	public Iterable<Book> books(final BookFilterInput filter) {
		if (filter == null) {
			return bookRepository.findAll();
		}
		return bookRepository.findAll(new Specification<Book>() {

			private static final long serialVersionUID = 1L;

			private void addPredicate(final BookFilterInput filter, final Root<Book> root, final CriteriaBuilder cb,
					final List<Predicate> predicates, final StringFilterInput stringFilterInput,
					final String columnName) {
				if (stringFilterInput.eq != null) {
					predicates.add(cb.equal(cb.lower(root.get(columnName)), stringFilterInput.getEq().toLowerCase()));
				}
				if (stringFilterInput.ne != null) {
					predicates.add(cb.notEqual(root.get(columnName), stringFilterInput.getNe()));
				}
				if (stringFilterInput.like != null) {
					predicates.add(cb.like(cb.lower(root.get(columnName)),
							"%" + stringFilterInput.getLike().toLowerCase() + "%"));
				}
			}

			@Override
			public Predicate toPredicate(final Root<Book> root, final CriteriaQuery<?> query,
					final CriteriaBuilder cb) {

				final List<Predicate> predicates = new ArrayList<>();

				if (filter.getTitle() != null) {
					addPredicate(filter, root, cb, predicates, filter.getTitle(), "title");
				}

				if (filter.getCategory() != null) {
					addPredicate(filter, root, cb, predicates, filter.getCategory(), "ccategory");
				}

				return cb.and(predicates.toArray(new Predicate[0]));
			}
		});
	}
}