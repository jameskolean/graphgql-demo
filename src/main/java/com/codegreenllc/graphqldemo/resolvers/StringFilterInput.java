package com.codegreenllc.graphqldemo.resolvers;

import lombok.Data;

@Data
public class StringFilterInput {
	String eq, ne, like;
}
