package com.codegreenllc.graphqldemo.repositories;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.codegreenllc.graphqldemo.entities.Author;

@Repository
public interface AuthorRepository extends CrudRepository<Author, UUID>, PagingAndSortingRepository<Author, UUID> {
}