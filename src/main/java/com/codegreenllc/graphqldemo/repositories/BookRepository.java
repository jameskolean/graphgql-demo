package com.codegreenllc.graphqldemo.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.codegreenllc.graphqldemo.entities.Book;

@Repository
public interface BookRepository
		extends CrudRepository<Book, UUID>, PagingAndSortingRepository<Book, UUID>, JpaSpecificationExecutor<Book> {
}