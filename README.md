
### Open the graphQL frontend
The graphiql will be at this location by default.
`http://localhost:8080/graphiql`

### Query to read all books
Run this query to list all the books

```javascript
{
  books {
    id
    title
    text
    category
  }
}
```

Filter books
```javascript
{
  books(filter: {title: {eq: "Joy Of Cooking"}}) {
    title
  }
}
```

Get Author too
```javascript
{
  books(filter: {title: {eq: "The Joy Of Cooking"}}) {
    title
    author{
      name
    }
  }
}
```

### Mutations
Run this to add a book to the database
```javascript
mutation {
  addBook(title: "My Best Seller", text: "https://nowhere.com/mybestseller", category: "fantasy") {
    title
    id
    text
    category
  }
}
```

Run this to update a book in the database
```javascript
mutation {
  updateBook(id: "00000000-0000-0000-0000-000000000001", title:"test") {
    id
    title
    text
    category
  }
}
```

Run this to delete a book from the database
```javascript
mutation {
  removeBook(id: "00000000-0000-0000-0000-000000000001")
}